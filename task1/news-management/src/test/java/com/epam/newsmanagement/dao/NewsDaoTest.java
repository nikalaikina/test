package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	TransactionDbUnitTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
  DbUnitTestExecutionListener.class })
public class NewsDaoTest {
	
	private final static Date DATE_EXAMPLE = new Date(10000000);
	private final static String DATABASE_SETUP = "classpath:database/database-setup.xml";
	
    @Autowired
    private DataSource dataSource;
    
    @Autowired
    private NewsDao newsDao;
    
    @Test
    @DatabaseSetup(value=DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/news-added.xml",  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAdd() throws DaoException {
    	News news =  new News("Short text", "Full text", "Title", DATE_EXAMPLE, DATE_EXAMPLE);
    	Long id = newsDao.insert(news);
    	assertTrue(id > 0);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/news-deleted.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testDelete() throws DaoException {
    	boolean ans = newsDao.delete(2L);
    	assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/news-updated.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdate() throws DaoException {
    	News news =  new News(2, "Short text", "Full text", "Title", DATE_EXAMPLE, DATE_EXAMPLE);
    	boolean ans = newsDao.update(news);
    	assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value=DATABASE_SETUP,  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetById() throws DaoException {
    	News news = newsDao.getById(3L);
    	assertTrue("breaking news3".equals(news.getShortText()));
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value=DATABASE_SETUP,  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetAll() throws DaoException {
    	List<News> news = newsDao.getAll();
    	assertEquals(news.size(), 4);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value=DATABASE_SETUP,  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetByAuthor() throws DaoException {
    	List<News> news = newsDao.getByAuthor(0L);
    	assertEquals(news.size(), 2);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value=DATABASE_SETUP,  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetByTag() throws DaoException {
    	List<News> news = newsDao.getByTag("foo");
    	assertEquals(news.size(), 2);
    }
}