package com.epam.newsmanagement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.CommonServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class CommonServiceTest {
	
	@Mock
	NewsService newsService;
	
	@Mock
	CommentService commentService;
	
	@Mock
	AuthorService authorService;
	
	@Mock
	TagService tagService;
	
	@InjectMocks
	@Autowired
	CommonServiceImpl commonService;
	
	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void addNewsTest() throws ServiceException {		
		News news = new News("Short", "Full", "Title", new Date(), new Date());
        Author  author = new Author("Dwight", new Date());
        List<String> tags = new ArrayList<String>();
        
        tags.add("lol");
        tags.add("ololo");
        tags.add("ahaha");
        
        commonService.addNews(news, author, tags);
        
        Mockito.verify(newsService).insert(news);
        Mockito.verify(authorService).insert(author);
	}

}
