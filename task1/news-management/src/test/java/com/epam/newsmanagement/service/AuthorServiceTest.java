package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.impl.AuthorServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class AuthorServiceTest {
	
	@Mock
	AuthorDao authorDao;
	
	@InjectMocks
	@Autowired
	AuthorServiceImpl authorService;
	
	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void testUpdate() throws Exception {		  
		Author author = new Author(1, "Auth", new Date());
		authorService.update(author);
  
		Mockito.verify(authorDao).update(author);		
	}

	@Test
	public void testDelete() throws Exception {
		authorService.delete(2L);
		  
		Mockito.verify(authorDao).delete(2L);			
	}

	@Test
	public void testGetById() throws Exception {
		Author author = new Author(2, "Auth", new Date());
		Mockito.when(authorDao.getById(2L)).thenReturn(author);
		
		Author returnedAuthor = authorService.getById(2L);
		  
		assertEquals(returnedAuthor, author);
		Mockito.verify(authorDao).getById(2L);			
	}

	@Test
	public void testInsert() throws Exception {
		Author author = new Author(2, "Auth", new Date());
		Mockito.when(authorDao.insert(author)).thenReturn(2L);
		
		Long authorId = authorService.insert(author);
		  
		assertTrue(authorId.equals(2L));
		Mockito.verify(authorDao).insert(author);
	}

}
