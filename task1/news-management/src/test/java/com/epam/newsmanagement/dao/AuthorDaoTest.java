package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	TransactionDbUnitTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
  DbUnitTestExecutionListener.class })
public class AuthorDaoTest {
	
	private final static Date DATE_EXAMPLE = new Date(10000000);
	private final static String DATABASE_SETUP = "classpath:database/database-setup.xml";
	
    @Autowired
    private DataSource dataSource;
    
    @Autowired
    private AuthorDao authorDao;
    
    @Test
    @DatabaseSetup(value = DATABASE_SETUP)
    @ExpectedDatabase(value = "classpath:database/author-added.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAdd() throws DaoException {
        Author  author = new Author("Dwight", DATE_EXAMPLE);
        Long id = authorDao.insert(author);
        assertTrue(id > 0);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value = "classpath:database/author-deleted.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testDelete() throws DaoException {
    	boolean ans = authorDao.delete(2L);
        assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value = "classpath:database/author-updated.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdate() throws DaoException {
        Author  author = new Author(0, "Dwight", DATE_EXAMPLE);
        boolean ans = authorDao.update(author);
        assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value = DATABASE_SETUP,  assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetById() throws DaoException {
    	Author author = authorDao.getById(1L);
    	assertTrue("Sam".equals(author.getName()));
    }
 
}
