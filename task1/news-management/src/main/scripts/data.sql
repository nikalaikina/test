
Insert into AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (2,'Leo',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (3,'Alex',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (0,'Bob',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into AUTHOR (AUTHOR_ID,AUTHOR_NAME,EXPIRED) values (1,'Sam',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));

Insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (2,2,'stupid comment',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (3,1,'short comment',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (4,0,'ont more comment',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (5,1,'op daun',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (0,1,'mycomment',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));
Insert into COMMENTS (COMMENT_ID,NEWS_ID,COMMENT_TEXT,CREATION_DATE) values (1,1,'angry comment',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'));

Insert into NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (2,'news','content','title',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('23-FEB-15','DD-MON-RR'));
Insert into NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (3,'breaking news3','newnewnewnew','new',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('23-FEB-15','DD-MON-RR'));
Insert into NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (0,'breaking news','newnewnewnew','new',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('23-FEB-15','DD-MON-RR'));
Insert into NEWS (NEWS_ID,SHORT_TEXT,FULL_TEXT,TITLE,CREATION_DATE,MODIFICATION_DATE) values (1,'breaking news2','text','Text',to_timestamp('15-MAR-15 10.55.55.000000000 AM','DD-MON-RR HH.MI.SSXFF AM'),to_date('23-FEB-15','DD-MON-RR'));

Insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (2,2);
Insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (3,3);
Insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (0,0);
Insert into NEWS_AUTHOR (NEWS_ID,AUTHOR_ID) values (1,0);

Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (2,0);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (3,0);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (0,1);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (0,2);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (0,3);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (0,0);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (2,2);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (2,3);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (0,0);
Insert into NEWS_TAG (NEWS_ID,TAG_ID) values (1,0);


Insert into TAG (TAG_ID,TAG_NAME) values (2,'foo');
Insert into TAG (TAG_ID,TAG_NAME) values (3,'food');
Insert into TAG (TAG_ID,TAG_NAME) values (0,'IT');
Insert into TAG (TAG_ID,TAG_NAME) values (1,'bar');