BEGIN
 BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE News_Author';
 EXCEPTION
    WHEN OTHERS THEN
       IF SQLCODE != -942 THEN
          RAISE;
       END IF;
 BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE News';
 EXCEPTION
    WHEN OTHERS THEN
       IF SQLCODE != -942 THEN
          RAISE;
       END IF;
 BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE Author';
 EXCEPTION
    WHEN OTHERS THEN
       IF SQLCODE != -942 THEN
          RAISE;
       END IF;
 BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE Comments';
 EXCEPTION
    WHEN OTHERS THEN
       IF SQLCODE != -942 THEN
          RAISE;
       END IF;
 BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE Tag';
 EXCEPTION
    WHEN OTHERS THEN
       IF SQLCODE != -942 THEN
          RAISE;
       END IF;
 BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE News_Tag';
 EXCEPTION
    WHEN OTHERS THEN
       IF SQLCODE != -942 THEN
          RAISE;
       END IF;    
 END;
 
Create sequence author_seq start with 1
increment by 1
minvalue 1
nomaxvalue;

create table Author (
 author_id number(20) not null,
 author_name varchar2(30) not null,
 expired timestamp not null,
 constraint pk_author primary key (author_id));

Create sequence comments_seq start with 1
increment by 1
minvalue 1
nomaxvalue;


create table Comments (
 comment_id number(20) not null,
 news_id number(20) not null,
 comment_text varchar2(100) not null,
 creation_date timestamp not null, 
 constraint pk_comment_id primary key (comment_id),
 constraint fk_comments_news_id foreign key (news_id) references News (news_id));


Create sequence news_seq start with 1
increment by 1
minvalue 1
nomaxvalue;

create table News (
 news_id number(20) not null,
 short_text varchar2(100) not null,
 full_text varchar2(2000) not null,
 title varchar2(30) not null,
 creation_date timestamp not null,
 modification_date date not null,
 constraint pk_news primary key (news_id));


create table News_Author (
 news_id number(20) not null,
 author_id number(20) not null,
 constraint fk_news_id foreign key (news_id) references News (news_id),
 constraint fk_author_id foreign key (author_id) references Author (author_id));

Create sequence tag_seq start with 1
increment by 1
minvalue 1
nomaxvalue;


create table Tag (
 tag_id number(20) not null,
 tag_name varchar2(30) not null,
 constraint pk_tag_id primary key (tag_id));


create table News_Tag (
 news_id number(20) not null,
 tag_id number(20) not null,
 constraint fk_news_tag_news_id foreign key (news_id) references News (news_id),
 constraint fk_tag_id foreign key (tag_id) references Tag (tag_id));
