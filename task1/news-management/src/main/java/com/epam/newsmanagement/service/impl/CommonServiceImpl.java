package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.CommonService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
@Transactional
public class CommonServiceImpl implements CommonService {

	private static Logger logger = Logger.getLogger(CommonServiceImpl.class);

	@Autowired
	private ServiceUtil serviceUtil;

	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private TagService tagService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	@Override
	@Transactional
	public void addNews(News news, Author author, List<String> tags)
			throws ServiceException {
		serviceUtil.assertNull(news);
		serviceUtil.assertNull(author);

		Long newsId = newsService.insert(news);
		Long authorId = authorService.insert(author);

		newsService.addAuthor(newsId, authorId);

		for (String tag : tags) {
			newsService.addTag(tag, newsId);
		}
	}

	@Override
	public List<News> getComplexNews(News news) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<News> getComplexNews(int newsId) {
		// TODO Auto-generated method stub
		return null;
	}

}
