package com.epam.newsmanagement.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.JdbcUtil;
import com.epam.newsmanagement.dao.ResultSetParser;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;

@Repository
public class AuthorDaoImpl implements AuthorDao {

	@Autowired
	private JdbcUtil jdbcUtil;
	
	private static final String DELETE = "DELETE FROM AUTHOR WHERE AUTHOR_ID=?";
	private static final String SELECT = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID=?";
	private static final String INSERT = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHOR_SEQ.NEXTVAL, ?, ?)";
	private static final String UPDATE = "UPDATE AUTHOR SET AUTHOR_NAME=?, EXPIRED=? WHERE AUTHOR_ID=?";
	
	public boolean delete(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.update(DELETE, Arrays.asList(values));		
	}
	
	public Author getById(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.get(SELECT, Arrays.asList(values), new ResultSetParser<Author>() {

			@Override
			public Author parseResultSet(ResultSet rs) throws SQLException {
				Author author = null;
				author = new Author( rs.getInt(1)
							       , rs.getString(2)
							       , rs.getDate(3));
				
				return author;
			}
		});
	}

	@Override
	public boolean update(Author author) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(author.getName());
		values.add(jdbcUtil.convertDate(author.getExpired()));
		values.add(author.getId());
		
		return jdbcUtil.update(UPDATE, values);
	}

	@Override
	public Long insert(Author author) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(author.getName());
		values.add(jdbcUtil.convertDate(author.getExpired()));
		
		return jdbcUtil.insert(INSERT, values, "author_id");
	}

	@Override
	public Author getByNews(Long newsId) {
		// TODO Auto-generated method stub
		return null;
	}
}
