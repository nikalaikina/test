package com.epam.newsmanagement.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @param <T> The type of entity.
 */
public interface ResultSetParser<T> {
	/**
	 * Parses an entity from result set.
	 * @param rs	result set with the row to parse
	 * @return		an entity
	 * @throws SQLException
	 */
	T parseResultSet(ResultSet rs) throws SQLException; 	
}
