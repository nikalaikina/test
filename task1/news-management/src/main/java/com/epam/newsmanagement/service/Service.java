package com.epam.newsmanagement.service;

import com.epam.newsmanagement.service.exception.ServiceException;


/**
 * CRUD operations with entities in persistence layer.
 * @param <T> the type of entity
 */
public interface Service<T> {

	/**
	 * Updates entity in database. 
	 * Identifies row for updating using an id.
	 * @param obj	an object for updating
	 * @throws ServiceException
	 */
	public void update(T obj) throws ServiceException;
	
	/**
	 * Deletes entity by id.
	 * @param id	id of entity to delete
	 * @throws ServiceException
	 */
	public void delete(Long id) throws ServiceException;
	
	/**
	 * Gets entity.
	 * @param id	id of entity
	 * @return		an entity object or null if it doesn't exist
	 * @throws ServiceException
	 */
	public T getById(Long id) throws ServiceException;
	
	/**
	 * Creates entity.
	 * @param obj	an entity to create
	 * @return		id of created entity or null if the operation wasn't successful
	 * @throws ServiceException
	 */
	public Long insert(T obj) throws ServiceException;
	
}
