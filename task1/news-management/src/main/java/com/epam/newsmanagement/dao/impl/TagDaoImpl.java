package com.epam.newsmanagement.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.JdbcUtil;
import com.epam.newsmanagement.dao.ResultSetParser;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;

@Repository
public class TagDaoImpl implements TagDao {

	@Autowired
	private JdbcUtil jdbcUtil = new JdbcUtil();
	
	private static final String DELETE = "DELETE FROM TAG WHERE TAG_ID=?";
	private static final String SELECT_BY_ID = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID=?";
	private static final String SELECT_BY_NAME = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_NAME=?";
	private static final String INSERT = "INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (TAG_SEQ.NEXTVAL, ?)";
	private static final String UPDATE = "UPDATE TAG SET TAG_NAME=? WHERE TAG_ID=?";
	
	ResultSetParser<Tag> rsp = new ResultSetParser<Tag>() {

		@Override
		public Tag parseResultSet(ResultSet rs) throws SQLException {
			Tag tag = null;		
			tag = new Tag( rs.getInt(1)
					     , rs.getString(2));
				
			return tag;
		}
	};
	
	public boolean delete(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.update(DELETE, Arrays.asList(values));		
	}
	
	public Tag getById(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.get(SELECT_BY_ID, Arrays.asList(values), rsp);
	}	

	@Override
	public Tag getByName(String name) throws DaoException {
		Object[] values = {name};
		
		return jdbcUtil.get(SELECT_BY_NAME, Arrays.asList(values), rsp);
	}
	
	@Override
	public boolean update(Tag tag) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(tag.getName());
		values.add(tag.getId());
		
		return jdbcUtil.update(UPDATE, values);
	}

	@Override
	public Long insert(Tag tag) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(tag.getName());
		
		return jdbcUtil.insert(INSERT, values, "tag_id");
	}

	@Override
	public List<Tag> getByNews(Long newsId) {
		// TODO Auto-generated method stub
		return null;
	}
}
