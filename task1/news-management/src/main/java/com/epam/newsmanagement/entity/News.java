package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

public class News implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long 	id;
	private String 	shortText;
	private String 	fulltext;
	private String 	title;
	private Date 	creationDate;
	private Date 	modificationDate;
	
	
	public News(long id, String shortText, String fulltext, String title,
			Date creationDate, Date modificationDate) {
		this.id = id;
		this.shortText = shortText;
		this.fulltext = fulltext;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}
	
	public News(String shortText, String fulltext, String title,
			Date creationDate, Date modificationDate) {
		this.shortText = shortText;
		this.fulltext = fulltext;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fulltext;
	}

	public void setFulltext(String fulltext) {
		this.fulltext = fulltext;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fulltext == null) ? 0 : fulltext.hashCode());
		result = prime * result + (int) id;
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		News other = (News) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fulltext == null) {
			if (other.fulltext != null)
				return false;
		} else if (!fulltext.equals(other.fulltext))
			return false;
		if (id != other.id)
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		
		return true;
	}	
	
	
}
