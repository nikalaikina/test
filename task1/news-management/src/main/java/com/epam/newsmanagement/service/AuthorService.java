package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Author;

public interface AuthorService extends Service<Author> {	

}
