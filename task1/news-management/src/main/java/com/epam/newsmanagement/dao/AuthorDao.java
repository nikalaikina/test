package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;

public interface AuthorDao extends Dao<Author> {	
	/**
	 * Gets author by news id.
	 * @param newsId
	 * @return
	 */
	public Author getByNews(Long newsId);
}
