package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 *
 */
public interface NewsService extends Service<News> {
	
	/**
	 * Gets all the news.
	 * @return 	list of News;
	 * @throws ServiceException
	 */
	public List<News> getAll() throws ServiceException;
	
	/**
	 * Gets all the news of one author.
	 * @param authorId	id of author
	 * @return			list of news
	 * @throws ServiceException
	 */
	public List<News> getByAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Adds author to a piece of news.
	 * @param authorId	id of author
	 * @param newsId	if of news
	 * @throws ServiceException
	 */
	public void addAuthor(Long authorId, Long newsId) throws ServiceException;
	
	/**
	 * Adds tag to a piece of news.
	 * @param tagId		tag id
	 * @param newsId	news id
	 * @throws ServiceException
	 */
	public void addTag(String tag, Long newsId) throws ServiceException;
	
	/**
	 * Gets all the news with the tag.
	 * @param tag	the tag
	 * @return	list of news
	 * @throws ServiceException
	 */
	public List<News> getByTags(List<String> tags) throws ServiceException;
	
}
