package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
public class NewsServiceImpl implements NewsService {

	private static Logger logger = Logger.getLogger(NewsServiceImpl.class);

	@Autowired
	private ServiceUtil serviseUtil;
	
	@Autowired
	private NewsDao newsDao;

	@Autowired
	private TagDao tagDao;	
	
	public void setNewsDao(NewsDao newsDao) {
		this.newsDao = newsDao;
	}

	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}
	
	@Override
	@Transactional
	public void update(News obj) throws ServiceException {
		serviseUtil.assertNull(obj);
		
		try {
			newsDao.update(obj);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void delete(Long id) throws ServiceException {
		try {
			newsDao.delete(id);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public News getById(Long id) throws ServiceException {
		try {
			return newsDao.getById(id);
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Long insert(News obj) throws ServiceException {
		serviseUtil.assertNull(obj);		
		
		try {
			return newsDao.insert(obj);
		} catch (DaoException e) {
			logger.error("DaoException while inserting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public List<News> getAll() throws ServiceException {
		try {
			return newsDao.getAll();
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public List<News> getByAuthor(Long authorId) throws ServiceException {
		try {
			return newsDao.getByAuthor(authorId);
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void addAuthor(Long authorId, Long newsId) throws ServiceException {
		try {
			newsDao.addAuthor(authorId, newsId);
		} catch (DaoException e) {
			logger.error("DaoException while adding", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void addTag(String tagName, Long newsId) throws ServiceException {		
		try {
			Tag tag = tagDao.getByName(tagName);
			
			if (tag == null) {
				throw new ServiceException("Trying to add nonexistent tag.");
			}
			
			Long tagId = tag.getId();			
			newsDao.addTag(tagId, newsId);
		} catch (DaoException e) {
			logger.error("DaoException while adding", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public List<News> getByTags(List<String> tags) throws ServiceException {
		try {
			return newsDao.getByTags(tags);
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}		
	}

}
