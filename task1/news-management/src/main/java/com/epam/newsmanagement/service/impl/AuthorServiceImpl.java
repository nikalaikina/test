package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.AuthorDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;


/**
 *
 */
@Service
public class AuthorServiceImpl implements AuthorService {
	
	private static Logger logger = Logger.getLogger(AuthorServiceImpl.class);

	@Autowired
	private ServiceUtil serviseUtil;
	
	@Autowired
	private AuthorDao authorDao;
	
	public void setAuthorDao(AuthorDao authorDao) {
		this.authorDao = authorDao;
	}

	@Override
	@Transactional
	public void update(Author obj) throws ServiceException {
		serviseUtil.assertNull(obj);
		
		try {
			authorDao.update(obj);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}
	}
	
	@Override
	@Transactional
	public void delete(Long id) throws ServiceException {
		try {
			authorDao.delete(id);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}	
	}

	@Override
	@Transactional
	public Author getById(Long id) throws ServiceException {
		try {
			return authorDao.getById(id);
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Long insert(Author obj) throws ServiceException {
		serviseUtil.assertNull(obj);
		
		try {
			return authorDao.insert(obj);
		} catch (DaoException e) {
			logger.error("DaoException while inserting", e);
			throw new ServiceException(e);
		}
	}
}
