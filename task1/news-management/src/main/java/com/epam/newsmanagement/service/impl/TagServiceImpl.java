package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
public class TagServiceImpl implements TagService {

	private static Logger logger = Logger.getLogger(TagServiceImpl.class);

	@Autowired
	private ServiceUtil serviceUtil;
	
	@Autowired
	private TagDao tagDao;
	
	public void setTagDao(TagDao tagDao) {
		this.tagDao = tagDao;
	}

	@Override
	@Transactional
	public void update(Tag obj) throws ServiceException {
		System.out.println(serviceUtil);
		System.out.println(tagDao);
		serviceUtil.assertNull(obj);
		
		try {
			tagDao.update(obj);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void delete(Long id) throws ServiceException {
		try {
			tagDao.delete(id);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Tag getById(Long id) throws ServiceException {
		try {
			return tagDao.getById(id);
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Long insert(Tag obj) throws ServiceException {
		if (obj == null) {
			logger.error("Trying insert null obj");
			return null;
		}
		
		try {
			return tagDao.insert(obj);
		} catch (DaoException e) {
			logger.error("DaoException while inserting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Tag getByName(String name) throws ServiceException {
		serviceUtil.assertNull(name);
		
		try {
			return tagDao.getByName(name);
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}
	}

}
