package com.epam.newsmanagement.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.JdbcUtil;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.ResultSetParser;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;

@Repository
public class NewsDaoImpl implements NewsDao {

	@Autowired
	private JdbcUtil jdbcUtil = new JdbcUtil();
	
	private static final String DELETE = 
			"DELETE FROM NEWS WHERE NEWS_ID=?";
	private static final String SELECT_BY_ID = 
			"SELECT NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE "
			+ "FROM NEWS WHERE NEWS_ID=?";
	private static final String SELECT_ORDERED_BY_POPULARITY = 
			"SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, "
			+ "NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS ORDER BY (SELECT "
			+ "COUNT(*) FROM COMMENTS WHERE NEWS.NEWS_ID=COMMENTS.NEWS_ID ) DESC";
	private static final String SELECT_BY_AUTHOR = 
			"SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, "
			+ "NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS FULL OUTER "
			+ "JOIN NEWS_AUTHOR ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID=?";
	private static final String SELECT_BY_TAG = 
			"SELECT NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, "
			+ "NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM NEWS INNER JOIN NEWS_TAG "
			+ "ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID=?";			
	private static final String INSERT = 
			"INSERT INTO NEWS " 
			+ "(NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) "
			+ "VALUES (NEWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
	private static final String UPDATE = 
			"UPDATE NEWS SET SHORT_TEXT=?, FULL_TEXT=?, TITLE=?, "
			+ "CREATION_DATE=?, MODIFICATION_DATE=? WHERE NEWS_ID=?";
	private static final String ADD_TAG =
			"INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES(?, ?)";
	private static final String ADD_AUTHOR =
			"INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES(?, ?)";
	
	@Autowired
	private TagDao tagDao;
	
	ResultSetParser<News> rsp = new ResultSetParser<News>() {
		
		@Override
		public News parseResultSet(ResultSet rs) throws SQLException {
			News news = null;	
			news = new News(rs.getInt(1)
				           ,rs.getString(2)
				           ,rs.getString(3)
				           ,rs.getString(4)
				           ,rs.getDate(5)
				           ,rs.getDate(6));
			
			return news;
		}		
	};
	
	public boolean delete(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.update(DELETE, Arrays.asList(values));		
	}
	
	public News getById(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.get(SELECT_BY_ID, Arrays.asList(values), rsp);
	}	


	public List<News> getAll() throws DaoException {
		return jdbcUtil.getList(SELECT_ORDERED_BY_POPULARITY
				 			   ,new ArrayList<Object>()
				 			   ,rsp);
	}
	
	public List<News> getByAuthor(Long authorId) throws DaoException {
		Object[] values = {authorId};
				
		return jdbcUtil.getList(SELECT_BY_AUTHOR
							   ,Arrays.asList(values)
							   ,rsp);
	}

	@Override
	public List<News> getByTag(String tag) throws DaoException {
		Long tagId = tagDao.getByName(tag).getId();
		System.out.println(tagId);
		Object[] values = {tagId};
		
		return jdbcUtil.getList(SELECT_BY_TAG
							   ,Arrays.asList(values)
							   ,rsp);
	}
	
	@Override
	public void addAuthor(Long authorId, Long newsId) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(newsId);
		values.add(authorId);
		
		jdbcUtil.update(ADD_AUTHOR, values);
	}

	@Override
	public void addTag(Long tagId, Long newsId) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(newsId);
		values.add(tagId);
		
		jdbcUtil.update(ADD_TAG, values);
		
	}	

	@Override
	public List<News> getByTags(List<String> tags) throws DaoException {
		List<News> news = new ArrayList<News>();
		
		for (String tag : tags) {
			List<News> newsWithCurrentTag = getByTag(tag);
			
			for (News newsIter : newsWithCurrentTag) {
				if (!news.contains(newsIter)) {
					news.add(newsIter);
				}
			}
		}
		
		return news;
	}

	@Override
	public boolean update(News news) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(news.getShortText());
		values.add(news.getFullText());
		values.add(news.getTitle());
		values.add(jdbcUtil.convertDate(news.getCreationDate()));
		values.add(jdbcUtil.convertDate(news.getModificationDate()));
		values.add(news.getId());
		
		return jdbcUtil.update(UPDATE, values);
	}

	@Override
	public Long insert(News news) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(news.getShortText());
		values.add(news.getFullText());
		values.add(news.getTitle());
		values.add(jdbcUtil.convertDate(news.getCreationDate()));
		values.add(jdbcUtil.convertDate(news.getModificationDate()));
		
		return jdbcUtil.insert(INSERT, values, "news_id");
	}

	@Override
	public List<News> getFiltered(List<String> tags, List<Long> authors)
			throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}	
}
