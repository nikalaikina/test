package com.epam.newsmanagement.dao.exception;


public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public DaoException(Exception e, String message) {
		super(message, e);
	}
	
	public DaoException(Exception e) {
		super(e);
	}
	
}
