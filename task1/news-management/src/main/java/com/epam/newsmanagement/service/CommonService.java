package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface CommonService {
	
	/**
	 * Creates entity of News with the author and tags.
	 * @param news		news entity
	 * @param author	author entity
	 * @param tags		list of tags
	 * @throws ServiceException
	 */
	public void addNews(News news, Author author, List<String> tags) throws ServiceException;

	public List<News> getComplexNews(News news);
	
	public List<News> getComplexNews(int newsId);
}
