package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
public class CommentServiceImpl implements CommentService {
	
	private static Logger logger = Logger.getLogger(CommentServiceImpl.class);

	@Autowired
	private ServiceUtil serviseUtil;

	@Autowired
	private CommentDao commentDao;
	
	public void setCommentDao(CommentDao commentDao) {
		this.commentDao = commentDao;
	}

	@Override
	@Transactional
	public void update(Comment obj) throws ServiceException {
		serviseUtil.assertNull(obj);
		
		try {
			commentDao.update(obj);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public void delete(Long id) throws ServiceException {
		try {
			commentDao.delete(id);
		} catch (DaoException e) {
			logger.error("DaoException while updating", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Comment getById(Long id) throws ServiceException {
		try {
			return commentDao.getById(id);
		} catch (DaoException e) {
			logger.error("DaoException while getting", e);
			throw new ServiceException(e);
		}
	}

	@Override
	@Transactional
	public Long insert(Comment obj) throws ServiceException {
		serviseUtil.assertNull(obj);
		
		try {
			return commentDao.insert(obj);
		} catch (DaoException e) {
			logger.error("DaoException while inserting", e);
			throw new ServiceException(e);
		}
	}

}
