package com.epam.newsmanagement.service;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface TagService extends Service<Tag> {
	
	/**
	 * Gets Tag entity by it's name;
	 * @param name 	the tag
	 * @return	the tag or null if it doesn't exist
	 * @throws ServiceException
	 */
	public Tag getByName(String name) throws ServiceException;
	
}
