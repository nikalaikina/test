package com.epam.newsmanagement.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.exception.DaoException;

@Component
public class JdbcUtil {
	
	@Autowired
	private DataSource dataSource;	
	
	public Long insert(String sql, List<Object> values, String returningValue) throws DaoException {
		ResultSet rs = null;
		Connection con = DataSourceUtils.getConnection(dataSource);
		
		try (PreparedStatement ps = con.prepareStatement(sql, new String[] { returningValue })) {			
			
			setValues(ps, values);	
			
			if(ps.executeUpdate() == 0) {
				return null;
			}
			
			rs = ps.getGeneratedKeys();
			rs.next();
			Long id = rs.getLong(1);		
			
			return id;
	
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeRS(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
	
	public boolean update(String sql, List<Object> values) throws DaoException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			
			setValues(ps, values);
			
			if(ps.executeUpdate() == 0) {
				return false;
			}
	
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		
		return true;
	}	
		
	public <T> T get(String sql, List<Object> values, ResultSetParser<T> rsp) throws DaoException {		
		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		T obj = null;
		
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			
			setValues(ps, values);
			rs = ps.executeQuery();
			
			rs.next();
			obj = rsp.parseResultSet(rs);
			
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeRS(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		 
		return obj;		
	}
	
	public <T> List<T> getList(String sql, List<Object> values, ResultSetParser<T> rsp) throws DaoException {
		Connection con = DataSourceUtils.getConnection(dataSource);
		ResultSet rs = null;
		List<T> objs = new ArrayList<T>();
		
		try (PreparedStatement ps = con.prepareStatement(sql)) {
			
			setValues(ps, values);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				objs.add(rsp.parseResultSet(rs));
			}
			
		} catch (SQLException e) {
			throw new DaoException(e);
		} finally {
			closeRS(rs);
			DataSourceUtils.releaseConnection(con, dataSource);
		}
		
		return objs;		
	}
	
	public java.sql.Date convertDate(java.util.Date date) {
		return new java.sql.Date(date.getTime());
	}	
	
	private void setValues(PreparedStatement ps, List<Object> values) throws SQLException {
		int i = 1;
		for(Object value : values) {
			ps.setObject(i++, value);
		}
	}
	
	private void closeRS(ResultSet rs) throws DaoException {
		if (rs != null) {		
			try {
				rs.close();
			} catch (SQLException e) {
				throw new DaoException(e);
			}			
		}
	}
}
