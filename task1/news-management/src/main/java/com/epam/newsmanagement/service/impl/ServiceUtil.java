package com.epam.newsmanagement.service.impl;

import org.springframework.stereotype.Component;

import com.epam.newsmanagement.service.exception.ServiceException;

@Component
public class ServiceUtil {

	public void assertNull(Object obj)
			throws ServiceException {
		if (obj == null) {
			throw new ServiceException("Argument is null.");
		}
	}
}
