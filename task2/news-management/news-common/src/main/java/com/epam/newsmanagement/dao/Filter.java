package com.epam.newsmanagement.dao;

import java.util.ArrayList;
import java.util.List;

public class Filter {
	private Long author;
	private List<Long> tags = new ArrayList<Long>();

	public Filter(Long author, List<Long> tags) {
		super();
		this.author = author;
		this.tags = tags;
	}
	
	public Long getAuthor() {
		return author;
	}
	
	public void setAuthor(Long authorId) {
		this.author = authorId;
	}
	
	public List<Long> getTags() {
		return tags;
	}
	
	public void addTag(Long tagId) {
		this.tags.add(tagId);
	}
	
	public void addTags(List<Long> tags) {
		if (tags != null) {
			this.tags.addAll(tags);
		}
	}


}
