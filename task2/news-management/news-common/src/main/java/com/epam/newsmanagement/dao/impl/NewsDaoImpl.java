package com.epam.newsmanagement.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.Filter;
import com.epam.newsmanagement.dao.JdbcUtil;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.ResultSetParser;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;

@Repository
public class NewsDaoImpl implements NewsDao {

	@Autowired
	private JdbcUtil jdbcUtil = new JdbcUtil();
	
	private static final String COLUMN_NAMES = " NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
			+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ";
	
	private static final String DELETE = 
			"DELETE FROM NEWS WHERE NEWS_ID=?";
	private static final String SELECT_BY_ID = 
			"SELECT" + COLUMN_NAMES + "FROM NEWS WHERE NEWS_ID=?";
	private static final String SELECT_ORDERED_BY_POPULARITY = 
			"SELECT" + COLUMN_NAMES + "FROM NEWS ORDER BY (SELECT "
			+ "COUNT(*) FROM COMMENTS WHERE NEWS.NEWS_ID=COMMENTS.NEWS_ID ) DESC";
	private static final String SELECT_BY_AUTHOR = 
			"SELECT" + COLUMN_NAMES + "FROM NEWS FULL OUTER JOIN NEWS_AUTHOR "
			+ "ON NEWS.NEWS_ID=NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID=?";
	private static final String SELECT_BY_TAG = 
			"SELECT" + COLUMN_NAMES + "FROM NEWS INNER JOIN NEWS_TAG "
			+ "ON NEWS.NEWS_ID=NEWS_TAG.NEWS_ID WHERE NEWS_TAG.TAG_ID=?";			
	private static final String INSERT = 
			"INSERT INTO NEWS (" + COLUMN_NAMES + ") VALUES (NEWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
	private static final String UPDATE = 
			"UPDATE NEWS SET SHORT_TEXT=?, FULL_TEXT=?, TITLE=?, "
			+ "CREATION_DATE=?, MODIFICATION_DATE=? WHERE NEWS_ID=?";
	private static final String ADD_TAG =
			"INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES(?, ?)";
	private static final String ADD_AUTHOR =
			"INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES(?, ?)";
	
	@Autowired
	private TagDao tagDao;
	
	@Autowired
	FilteringQueryMaker filteringQueryMaker = new FilteringQueryMaker();
	
	ResultSetParser<News> rsp = new ResultSetParser<News>() {		
		@Override
		public News parseResultSet(ResultSet rs) throws SQLException {
			News news = null;	
			news = new News(rs.getInt(1)
				           ,rs.getString(2)
				           ,rs.getString(3)
				           ,rs.getString(4)
				           ,rs.getDate(5)
				           ,rs.getDate(6));
			
			return news;
		}		
	};
	
	ResultSetParser<Integer> integerRsp = new ResultSetParser<Integer>() {		
		@Override
		public Integer parseResultSet(ResultSet rs) throws SQLException {
			return rs.getInt(1);
		}		
	};
	
	public boolean delete(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.update(DELETE, Arrays.asList(values));		
	}
	
	public News getById(Long id) throws DaoException {
		Object[] values = {id};
		
		return jdbcUtil.get(SELECT_BY_ID, Arrays.asList(values), rsp);
	}	


	public List<News> getAll() throws DaoException {
		return jdbcUtil.getList(SELECT_ORDERED_BY_POPULARITY
				 			   ,new ArrayList<Object>()
				 			   ,rsp);
	}
	
	public List<News> getByAuthor(Long authorId) throws DaoException {
		Object[] values = {authorId};
				
		return jdbcUtil.getList(SELECT_BY_AUTHOR
							   ,Arrays.asList(values)
							   ,rsp);
	}

	@Override
	public List<News> getByTag(String tag) throws DaoException {
		Long tagId = tagDao.getByName(tag).getId();
		Object[] values = {tagId};
		
		return jdbcUtil.getList(SELECT_BY_TAG
							   ,Arrays.asList(values)
							   ,rsp);
	}
	
	@Override
	public void addAuthor(Long authorId, Long newsId) throws DaoException {
		Object[] values = {newsId, authorId};
		
		jdbcUtil.update(ADD_AUTHOR, Arrays.asList(values));
	}

	@Override
	public void addTag(Long tagId, Long newsId) throws DaoException {
		Object[] values = {newsId, tagId};
		
		jdbcUtil.update(ADD_TAG, Arrays.asList(values));
		
	}	

	@Override
	public List<News> getByTags(List<String> tags) throws DaoException {
		List<News> news = new ArrayList<News>();
		
		for (String tag : tags) {
			List<News> newsWithCurrentTag = getByTag(tag);
			
			for (News newsIter : newsWithCurrentTag) {
				if (!news.contains(newsIter)) {
					news.add(newsIter);
				}
			}
		}
		
		return news;
	}

	@Override
	public boolean update(News news) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(news.getShortText());
		values.add(news.getFullText());
		values.add(news.getTitle());
		values.add(jdbcUtil.convertDate(news.getCreationDate()));
		values.add(jdbcUtil.convertDate(news.getModificationDate()));
		values.add(news.getId());
		
		return jdbcUtil.update(UPDATE, values);
	}

	@Override
	public Long insert(News news) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(news.getShortText());
		values.add(news.getFullText());
		values.add(news.getTitle());
		values.add(jdbcUtil.convertDate(news.getCreationDate()));
		values.add(jdbcUtil.convertDate(news.getModificationDate()));
		
		return jdbcUtil.insert(INSERT, values, "news_id");
	}

	@Override
	public List<News> getFiltered(Filter filter, int page, int pageSize) throws DaoException {
		List<Object> values = new ArrayList<Object>(filter.getTags().size() + 2);
		String sql = filteringQueryMaker.prepareEntityQuery( filter
														   , values
														   , page
														   , pageSize);
		
		return jdbcUtil.getList(sql, values, rsp);
	}	

	@Override
	public Integer getNumber(Filter filter, int pageSize) throws DaoException {
		List<Object> values = new ArrayList<Object>(filter.getTags().size() + 2);
		String sql = filteringQueryMaker.prepareNumberQuery(filter, values);
		
		return jdbcUtil.get(sql, values, integerRsp);
	}
	
}
