package com.epam.newsmanagement.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.dao.JdbcUtil;
import com.epam.newsmanagement.dao.ResultSetParser;
import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;

@Repository
public class CommentDaoImpl implements CommentDao {

	@Autowired
	private JdbcUtil jdbcUtil = new JdbcUtil();
	
	private static final String DELETE = "DELETE FROM COMMENTS WHERE COMMENT_ID=?";
	private static final String SELECT = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID=?";
	private static final String INSERT = "INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) VALUES (COMMENTS_SEQ.NEXTVAL, ?, ?, ?)";
	private static final String UPDATE = "UPDATE COMMENTS SET COMMENT_TEXT=?, CREATION_DATE=? WHERE COMMENT_ID=?";
	private static final String SELECT_BY_NEWS = "SELECT * FROM COMMENTS WHERE NEWS_ID=?";
	
	public boolean delete(Long id) throws DaoException {
		Object[] values = {id};

		return jdbcUtil.update(DELETE, Arrays.asList(values));
		
	}
	
	ResultSetParser<Comment> rsp = new ResultSetParser<Comment>() {
		
		@Override
		public Comment parseResultSet(ResultSet rs) throws SQLException {
			Comment comment = null;		
			comment = new Comment( rs.getInt(1)
					             , rs.getString(3)
					             , rs.getDate(4)
					             , rs.getInt(2));	
			
			return comment;
		}	
	};
	
	public Comment getById(Long id) throws DaoException {
		Object[] values = {id};

		return jdbcUtil.get(SELECT, Arrays.asList(values), rsp);
	}

	@Override
	public boolean update(Comment comment) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(comment.getText());
		values.add(jdbcUtil.convertDate(comment.getDate()));
		values.add(comment.getId());
		
		return jdbcUtil.update(UPDATE, values);
	}


	@Override
	public Long insert(Comment comment) throws DaoException {
		List<Object> values = new ArrayList<Object>();
		
		values.add(comment.getText());
		values.add(jdbcUtil.convertDate(comment.getDate()));
		values.add(comment.getNewsId());
		
		return jdbcUtil.insert(INSERT, values, "comment_id");
	}

	@Override
	public List<Comment> getByNews(Long newsId) throws DaoException {
		Object[] values = {newsId};

		return jdbcUtil.getList(SELECT_BY_NEWS, Arrays.asList(values), rsp);
	}
}
