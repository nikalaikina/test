package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;


public interface TagDao extends Dao<Tag> {
	
	/**
	 * Gets Tag entity by it's name;
	 * @param name 	the tag
	 * @return	the tag or null if it doesn't exist
	 * @throws DaoException
	 */
	public Tag getByName(String name) throws DaoException; 
	
	/**
	 * Gets list of tags by news id.
	 * @param newsId
	 * @return list of Tag
	 * @throws DaoException
	 */
	public List<Tag> getByNews(Long newsId) throws DaoException;
	
	/**
	 * Gets all the tags.
	 * @return 	list of Tag;
	 * @throws DaoException
	 */
	public List<Tag> getAll() throws DaoException ;
} 