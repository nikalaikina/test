package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
	
	private static final long serialVersionUID = 1L;
		
	private long 	id;
	private String 	text;
	private Date 	date;
	private long 	newsId;
	
	
	public Comment(long id, String text, Date date, long newsId) {
		this.id = id;
		this.text = text;
		this.date = date;
		this.newsId = newsId;
	}
	
	public Comment(String text, Date date, long newsId) {
		this.text = text;
		this.date = date;
		this.newsId = newsId;
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getNewsId() {
		return newsId;
	}

	public void setNews_id(long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + (int) id;
		result = prime * result + (int) newsId;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Comment other = (Comment) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id != other.id)
			return false;
		if (newsId != other.newsId)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		
		return true;
	}
	
	
	
}
