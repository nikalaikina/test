package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface TagService extends Service<Tag> {
	
	/**
	 * Gets Tag entity by it's name;
	 * @param name 	the tag
	 * @return	Tag or null if it doesn't exist
	 * @throws ServiceException
	 */
	public Tag getByName(String name) throws ServiceException;
	
	/**
	 * Gets list of tags by news id.
	 * @param newsId
	 * @return list of Tag
	 * @throws ServiceException
	 */
	public List<Tag> getByNews(Long newsId) throws ServiceException;
	
	/**
	 * Gets all the tags.
	 * @return 	list of Tag;
	 * @throws ServiceException
	 */
	public List<Tag> getAll() throws ServiceException;
}
