package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Author;

public interface AuthorDao extends Dao<Author> {	
	
	/**
	 * Gets author by news id.
	 * @param newsId
	 * @return
	 * @throws DaoException
	 */
	public Author getByNews(Long newsId) throws DaoException;
	
	/**
	 * Gets all the authors.
	 * @return 	list of Author;
	 * @throws DaoException
	 */
	public List<Author> getAll() throws DaoException ;
}
