package com.epam.newsmanagement.dao;

public class PageRequest {
	
	private Integer page;
	private Integer size;
	private Filter filter;
	
	public PageRequest(Integer page, Integer size, Filter filter) {
		this.page = page;
		this.size = size;
		this.filter = filter;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Filter getFilter() {
		return filter;
	}
}
