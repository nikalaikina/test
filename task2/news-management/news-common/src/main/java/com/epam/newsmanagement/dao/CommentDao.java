package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;


public interface CommentDao extends Dao<Comment> {
		
	/**
	 * Gets list of comments by news id.
	 * @param newsId
	 * @return list of Comment
	 * @throws DaoException
	 */
	public List<Comment> getByNews(Long newsId) throws DaoException;
}
