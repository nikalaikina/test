package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface CommentService extends Service<Comment> {
	
	/**
	 * Gets list of comments by news id.
	 * @param newsId
	 * @return list of Comment
	 * @throws ServiceException
	 */
	public List<Comment> getByNews(Long newsId) throws ServiceException;
}
