package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.Filter;
import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.ComplexNews;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.CommonService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Service
@Transactional
public class CommonServiceImpl implements CommonService {

	private static Logger logger = Logger.getLogger(CommonServiceImpl.class);

	@Autowired
	private ServiceUtil serviceUtil;

	@Autowired
	private NewsService newsService;

	@Autowired
	private AuthorService authorService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private TagService tagService;

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	@Override
	@Transactional
	public void addNews(News news, Author author, List<String> tags)
			throws ServiceException {
		serviceUtil.assertNull(news);
		serviceUtil.assertNull(author);

		Long newsId = newsService.insert(news);
		Long authorId = authorService.insert(author);

		newsService.addAuthor(newsId, authorId);

		for (String tag : tags) {
			newsService.addTag(tag, newsId);
		}
	}

	@Override
	public List<ComplexNews> getComplexNews(Filter filter, int page, int pageSize) throws ServiceException {
		List<News> news = newsService.getFiltered(filter, page, pageSize);
		
		List<ComplexNews> complexNews = new ArrayList<ComplexNews>(news.size());
		
		for (News aNews : news) {
			complexNews.add(getComplexNews(aNews));
		}
		
		return complexNews;
	}

	@Override
	public ComplexNews getComplexNews(News news) throws ServiceException {
		List<Tag> tags = tagService.getByNews(news.getId());
		Author author = authorService.getByNews(news.getId());
		List<Comment> comments = commentService.getByNews(news.getId());
		
		return new ComplexNews(news, author, tags, comments);
	}

}
