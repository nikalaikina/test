package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ComplexNews implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private News news;
	private Author author;
	private List<Tag> tags;
	private List<Comment> comments;	
	
	public ComplexNews(News news, Author author, List<Tag> tags, List<Comment> comments) {
		this.news = news;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}
	
	public long getId() {
		return news.getId();
	}

	public String getShortText() {
		return news.getShortText();
	}

	public String getFullText() {
		return news.getFullText();
	}

	public String getTitle() {
		return news.getTitle();
	}
	
	public Date getCreationDate() {
		return news.getCreationDate();
	}

	public Date getModificationDate() {
		return news.getModificationDate();
	}
	
	

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public News getNews() {
		return news;
	}
	
	public void setNews(News news) {
		this.news = news;
	}
	
	public List<Tag> getTags() {
		return tags;
	}
	
	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}
	
	public List<Comment> getComments() {
		return comments;
	}
	
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		
		int result = 1;
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result + ((news == null) ? 0 : news.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		ComplexNews other = (ComplexNews) obj;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (news == null) {
			if (other.news != null)
				return false;
		} else if (!news.equals(other.news))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		
		return true;
	}	
	
	
}
