package com.epam.newsmanagement.run;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.service.CommonService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

@Component
public class Main {
	
	@Autowired
	CommonService nService;
	
	public void execute() throws ServiceException {
//		News news = new News("transaction6", "Full", "Title", new Date(), new Date());
//        Author  author = new Author("transaction7", new Date());
//        List<String> tags = new ArrayList<String>();
//        
//        nService.addNews(news, author, tags);	
//        
        TagService ts = new TagServiceImpl();
		ts.update(null);
	}
	
}
