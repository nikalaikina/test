package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.dao.exception.DaoException;


/**
 * CRUD operations with entities in database.
 * @param <T> the type of entity
 */
public interface Dao<T> {

	/**
	 * Updates entity in database. 
	 * Identifies row for updating using an id.
	 * @param obj	an object for updating
	 * @return 		success of the operation
	 * @throws DaoException
	 */
	public boolean update(T obj) throws DaoException;
	
	/**
	 * Deletes entity by id.
	 * @param id	id of entity to delete
	 * @return		success of the operation
	 * @throws DaoException
	 */
	public boolean delete(Long id) throws DaoException;
	
	/**
	 * Gets entity.
	 * @param id	id of entity
	 * @return		an entity object or null if it doesn't exist
	 * @throws DaoException
	 */
	public T getById(Long id) throws DaoException;
	
	/**
	 * Creates entity.
	 * @param obj	an entity to create
	 * @return		id of created entity or null if the operation wasn't successful
	 * @throws DaoException
	 */
	public Long insert(T obj) throws DaoException;

}
