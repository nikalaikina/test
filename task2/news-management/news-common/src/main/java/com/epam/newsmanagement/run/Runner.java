package com.epam.newsmanagement.run;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.service.exception.ServiceException;

public class Runner {
	public static void main(String[] args) throws ServiceException {
		ApplicationContext context = 
				new ClassPathXmlApplicationContext("spring.xml");
		Main obj = (Main) context.getBean(Main.class);
	    obj.execute();
	    
		
	}
}
