package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.Filter;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.ComplexNews;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface CommonService {
	
	/**
	 * Creates entity of News with the author and tags.
	 * @param news		news entity
	 * @param author	author entity
	 * @param tags		list of tags
	 * @throws ServiceException
	 */
	public void addNews(News news, Author author, List<String> tags) throws ServiceException;

	public List<ComplexNews> getComplexNews(Filter filter, int page, int pageSize) throws ServiceException;
	
	public ComplexNews getComplexNews(News news) throws ServiceException;

}
