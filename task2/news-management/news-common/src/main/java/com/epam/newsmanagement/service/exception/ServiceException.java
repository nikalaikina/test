package com.epam.newsmanagement.service.exception;

public class ServiceException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ServiceException(Exception e, String message) {
		super(message, e);
	}
	
	public ServiceException(Exception e) {
		super(e);
	}
	
	public ServiceException(String message) {
		super(message);
	}
	

}
