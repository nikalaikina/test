package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.exception.ServiceException;

public interface AuthorService extends Service<Author> {	
	
	/**
	 * Gets author by news id.
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public Author getByNews(Long newsId) throws ServiceException;

	/**
	 * Gets all the authors.
	 * @return 	list of Author;
	 * @throws ServiceException
	 */
	public List<Author> getAll() throws ServiceException;
}
