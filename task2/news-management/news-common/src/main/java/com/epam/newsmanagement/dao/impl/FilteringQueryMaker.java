package com.epam.newsmanagement.dao.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import com.epam.newsmanagement.dao.Filter;

@Component
class FilteringQueryMaker {
	
	private static final String ORDER_BY = "ORDER BY (SELECT COUNT (*) FROM COMMENTS WHERE COMMENTS.NEWS_ID=NEWS.NEWS_ID)) ";
	private static final String FOR_PAGE = "WHERE RN BETWEEN ? AND ? ";
	private static final String COLUMN_NAMES = 
			" NEWS.NEWS_ID, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, "
			+ "NEWS.TITLE, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE ";
	
	private static final String AUTHOR_FILTERING = 
			"INNER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID "
			+ "WHERE NEWS_AUTHOR.AUTHOR_ID = ?";
	
	public String prepareNumberQuery(Filter filter, List<Object> values) {
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM NEWS ");
		
		sql.append(makeFiltering(filter, values));		
		
		return sql.toString();
	}
	
	public String prepareEntityQuery(Filter filter, List<Object> values, int page, int pageSize) {
		StringBuilder sql = new StringBuilder("SELECT * FROM ( SELECT " + COLUMN_NAMES + ", ROWNUM RN FROM NEWS ");
		
		sql.append(makeFiltering(filter, values));	
		sql.append(ORDER_BY);	
		sql.append(FOR_PAGE);	
			
		int startPage = (page - 1) * pageSize + 1;
		
		values.add(startPage);
		values.add(startPage + pageSize);
		
		return sql.toString();
	}
	
	private String makeFiltering(Filter filter, List<Object> values) {
		List<Long> tags = filter.getTags();
		Long authorId = filter.getAuthor();
		
		StringBuilder heart = new StringBuilder();
		
		if (authorId != null) {
			heart.append(AUTHOR_FILTERING);
			values.add(authorId);
		} 
		
		if (tags.isEmpty()) {			
			return heart.toString();
		}
		
		heart.append((authorId == null) ? " WHERE " : " AND ");		
		heart.append("(SELECT COUNT(*) FROM NEWS_TAG WHERE NEWS.NEWS_ID=NEWS_TAG.NEWS_ID AND NEWS_TAG.TAG_ID in (");
			
		for(int i = 1; i < tags.size(); i++) {
			heart.append("?, ");
		}
		heart.append("?))=? ");
		
		values.addAll(tags);
		values.add(tags.size());
		
		return heart.toString();
	}	
}
