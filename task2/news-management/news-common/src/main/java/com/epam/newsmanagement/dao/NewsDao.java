package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.News;

public interface NewsDao extends Dao<News> {	
	
	/**
	 * Gets all the news.
	 * @return 	list of News;
	 * @throws DaoException
	 */
	public List<News> getAll() throws DaoException ;
	
	/**
	 * Gets all the news of one author.
	 * @param authorId	id of author
	 * @return			list of news
	 * @throws DaoException
	 */
	public List<News> getByAuthor(Long authorId) throws DaoException;

	/**
	 * @param tags		list of tags in String
	 * @return			list of news
	 * @throws DaoException
	 */
	public List<News> getByTags(List<String> tags) throws DaoException;
	
	/**
	 * Adds author to a piece of news.
	 * @param authorId	id of author
	 * @param newsId	if of news
	 * @throws DaoException
	 */
	public void addAuthor(Long authorId, Long newsId) throws DaoException;
	
	/**
	 * Adds tag to a piece of news.
	 * @param tagId		tag id
	 * @param newsId	news id
	 * @throws DaoException
	 */
	public void addTag(Long tagId, Long newsId) throws DaoException;
	
	/**
	 * Gets all the news with the tag.
	 * @param tag	the tag
	 * @return	list of news
	 * @throws DaoException
	 */
	public List<News> getByTag(String tag) throws DaoException;
	
	/**
	 * Gets all the news with the tag.
	 * @param tag	the tag
	 * @return	list of news
	 * @throws DaoException
	 */	
	public List<News> getFiltered(Filter filter, int page, int pageSize) throws DaoException;
	
	/**
	 * Gets the number of news fitting the filter.
	 * @param filter
	 * @return
	 * @throws DaoException
	 */
	public Integer getNumber(Filter filter, int pageSize) throws DaoException;
}
