package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.CommentDao;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.impl.CommentServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class CommentServiceTest {
	
	@Mock
	CommentDao commentDao;
	
	@InjectMocks
	@Autowired
	CommentServiceImpl commentService;
	
	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
 
	@Test
	public void testUpdate() throws Exception {		  
		Comment comment = new Comment(1, "Auth", new Date(), 0);
		commentService.update(comment);
  
		Mockito.verify(commentDao).update(comment);		
	}

	@Test
	public void testDelete() throws Exception {
		commentService.delete(2L);
		  
		Mockito.verify(commentDao).delete(2L);			
	}

	@Test
	public void testGetById() throws Exception {
		Comment comment = new Comment(2, "Auth", new Date(), 0);
		Mockito.when(commentDao.getById(2L)).thenReturn(comment);
		
		Comment returnedComment = commentService.getById(2L);
		  
		assertEquals(returnedComment, comment);
		Mockito.verify(commentDao).getById(2L);			
	}

	@Test
	public void testInsert() throws Exception {
		Comment comment = new Comment(2, "Auth", new Date(), 0);
		Mockito.when(commentDao.insert(comment)).thenReturn(2L);
		
		Long commentId = commentService.insert(comment);
		  
		assertTrue(commentId.equals(2L));
		Mockito.verify(commentDao).insert(comment);
	}

}
