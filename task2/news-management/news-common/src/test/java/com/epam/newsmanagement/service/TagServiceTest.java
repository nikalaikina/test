package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.impl.TagServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class TagServiceTest {

	@Mock
	TagDao tagDao;

	@InjectMocks
	@Autowired
	TagServiceImpl tagService;
	
	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
 
	
	@Test
	public void testUpdate() throws Exception {
		Tag tag = new Tag(1, "Auth");
		
		System.out.println(tag);
		System.out.println(tagService);

		tagService.update(tag);

		Mockito.verify(tagDao).update(tag);
	}

	@Test
	public void testDelete() throws Exception {
		tagService.delete(2L);

		Mockito.verify(tagDao).delete(2L);
	}

	@Test
	public void testGetById() throws Exception {
		Tag tag = new Tag(2, "Auth");
		Mockito.when(tagDao.getById(2L)).thenReturn(tag);

		Tag returnedTag = tagService.getById(2L);

		assertEquals(returnedTag, tag);
		Mockito.verify(tagDao).getById(2L);
	}

	@Test
	public void testInsert() throws Exception {
		Tag tag = new Tag(2, "Auth");
		Mockito.when(tagDao.insert(tag)).thenReturn(new Long(2));

		Long tagId = tagService.insert(tag);

		assertTrue(tagId.equals(2L));
		Mockito.verify(tagDao).insert(tag);
	}
}
