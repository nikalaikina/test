package com.epam.newsmanagement.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.NewsDao;
import com.epam.newsmanagement.dao.TagDao;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.impl.NewsServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
public class NewsServiceTest {
	
	@Mock
	NewsDao newsDao;
	
	@Mock
	TagDao tagDao;
	
	@InjectMocks
	@Autowired
	NewsServiceImpl newsService;

	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void testUpdate() throws Exception {		
		News news = new News(2, "Short", "Full", "Title", new Date(), new Date());
		newsService.update(news);
  
		Mockito.verify(newsDao).update(news);		
	}

	@Test
	public void testDelete() throws Exception {
		newsService.setNewsDao(newsDao);
		newsService.delete(2L);
		  
		Mockito.verify(newsDao).delete(2L);			
	}

	@Test
	public void testGetById() throws Exception {
		News news = new News(2, "Short", "Full", "Title", new Date(), new Date());
		Mockito.when(newsDao.getById(2L)).thenReturn(news);
		
		News returnedNews = newsService.getById(2L);
		  
		assertEquals(returnedNews, news);
		Mockito.verify(newsDao).getById(2L);			
	}

	@Test
	public void testInsert() throws Exception {
		News news = new News(1, "Short", "Full", "Title", new Date(), new Date());
		Mockito.when(newsDao.insert(news)).thenReturn(2L);
		
		Long newsId = newsService.insert(news);
		  
		assertTrue(newsId.equals(2L));
		Mockito.verify(newsDao).insert(news);
	}

}
