package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Tag;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	TransactionDbUnitTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
  DbUnitTestExecutionListener.class })
public class TagDaoTest {
	
	private final static String DATABASE_SETUP = "classpath:database/database-setup.xml";
	
    @Autowired
    private DataSource dataSource;
    
    @Autowired
    private TagDao tagDao;
    
    @Test
    @DatabaseSetup(value=DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/tag-added.xml",  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAdd() throws DaoException {
    	Tag tag =  new Tag("java");
        Long id = tagDao.insert(tag);
        assertTrue(id > 0);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/tag-deleted.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testDelete() throws DaoException {
        boolean ans = tagDao.delete(2L);
        assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/tag-updated.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdate() throws DaoException {
    	Tag tag =  new Tag(0, "ITT");
        boolean ans = tagDao.update(tag);
        assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value=DATABASE_SETUP,  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetById() throws DaoException {
    	Tag tag = tagDao.getById(1L);
    	assertTrue("bar".equals(tag.getName()));
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value=DATABASE_SETUP,  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetByName() throws DaoException {
    	Tag tag = tagDao.getByName("bar");
    	assertEquals(1, tag.getId());
    }
    
}