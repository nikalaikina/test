package com.epam.newsmanagement.dao;

import static org.junit.Assert.assertTrue;

import java.util.Date;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;

import com.epam.newsmanagement.dao.exception.DaoException;
import com.epam.newsmanagement.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
	TransactionDbUnitTestExecutionListener.class,
	DirtiesContextTestExecutionListener.class,
  DbUnitTestExecutionListener.class })
public class CommentDaoTest {
	
	private final static Date DATE_EXAMPLE = new Date(10000000);
	private final static String DATABASE_SETUP = "classpath:database/database-setup.xml";
	
    @Autowired
    private DataSource dataSource;
    
    @Autowired
    private CommentDao commentDao;
    
    @Test
    @DatabaseSetup(value=DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/comment-added.xml",  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testAdd() throws DaoException {
    	Comment comment =  new Comment("lol", DATE_EXAMPLE, 1);
    	Long id = commentDao.insert(comment);
    	assertTrue(id > 0);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/comment-deleted.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testDelete() throws DaoException {
    	boolean ans = commentDao.delete(2L);
    	assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value="classpath:database/comment-updated.xml", assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdate() throws DaoException {
    	Comment comment =  new Comment(1, "kek", DATE_EXAMPLE, 1);
    	boolean ans = commentDao.update(comment);
    	assertTrue(ans);
    }
    
    @Test
    @DatabaseSetup(DATABASE_SETUP)
    @ExpectedDatabase(value=DATABASE_SETUP,  assertionMode=DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetById() throws DaoException {
    	Comment comment = commentDao.getById(1L);
    	assertTrue("angry comment".equals(comment.getText()));
    }
    
}
