<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>News portal</title>
	<style type="text/css">
		.component {
			border: 2px solid;
		}
		.body {
			width: 100%;
			min-height: 680px;
		}
		.footer {
			clear:both;
		}
	</style>
</head>
<body>
	<div>
		<!-- Header -->
		<div class=component>
			<tiles:insertAttribute name="header" />
		</div>
		<!-- Body Page -->
		<div class="component body">
			<tiles:insertAttribute name="body" />
		</div>
		
		<!-- Footer Page -->
		<div class=component style="text-align:center">
			<tiles:insertAttribute name="footer" />
		</div>
	</div>
</body>
</html>