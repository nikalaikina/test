<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
.multiselect {
	width: 200px;
}

.selectBox {
	position: relative;
}

.selectBox select {
	width: 100%;
	font-weight: bold;
}

.overSelect {
	position: absolute;
	left: 0;
	right: 0;
	top: 0;
	bottom: 0;
}

#checkboxes {
	display: none;
	border: 1px #dadada solid;
}

#checkboxes label {
	display: block;
}

#checkboxes label:hover {
	background-color: #1e90ff;
}
div.inline { float:left; }

</style>
<script>
	var expanded = false;
	function showCheckboxes() {
		var checkboxes = document.getElementById("checkboxes");
		if (!expanded) {
			checkboxes.style.display = "block";
			expanded = true;
		} else {
			checkboxes.style.display = "none";
			expanded = false;
		}
	}
</script>
<tiles:insertDefinition name="DefaultTemplate">
	<tiles:putAttribute name="body">
		<form action="home" method="post">
			<div class="filter">
				<select name="authorId" style="float: left;">
					<c:forEach items="${authors}" var="author">
						<option value="${author.id}">${author.name}</option>
					</c:forEach>
				</select> 
				
				<div class="multiselect" style="float: left;">
					<div class="selectBox" onclick="showCheckboxes()" style="float: left;">
						<select>
							<option>Select an option</option>
						</select>
						<div class="overSelect"></div>
					</div>
					
					<div id="checkboxes" style="float: left;">
						<c:forEach items="${tags}" var="tag">							
							
							<label>
								<input type="checkbox" id="${tag.id}" />${tag.name}
							</label> 
							
						</c:forEach>
					</div>
				</div>
				
				<input type="submit" value="Filter"> 
				<input type="submit" value="Reset">
			</div>

			<div>
				<c:forEach items="${news_list}" var="news">							
					<b>${news.title}</b>  (by <span>${news.author}</span>) 
					<br>
				</c:forEach>
			</div>

		</form>
		<c:forEach begin="1" end="${pages}" var="page">
    		<input type="button" value="${page}" onclick="location.href='home?page=${page}';" />
		</c:forEach>
		<br>
	</tiles:putAttribute>
</tiles:insertDefinition>