package com.epam.newsmanagement.controller;

import java.util.Arrays;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagement.dao.Filter;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommonService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;


@Controller
public class HomeController {
	
	@Autowired
	NewsService newsService;
	
	@Autowired
	AuthorService authorService;
	
	@Autowired
	TagService tagService;
	
	@Autowired
	CommonService commonService;
	
	@RequestMapping(value = { "/home", "/" })
	public String home(Locale locale, Model model,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false) Long authorId,
			@RequestParam(required = false, defaultValue = "") Long[] tagsId) throws ServiceException {
		
		Filter filter = new Filter(authorId, Arrays.asList(tagsId));
				
		model.addAttribute("news_list", commonService.getComplexNews(filter, page, 3));
		model.addAttribute("authors", authorService.getAll());
		model.addAttribute("tags", tagService.getAll());
		model.addAttribute("pages", newsService.getPagesNumber(filter, 3));

		return "home";
	}
}
